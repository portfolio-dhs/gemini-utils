package com.gemini_is.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class SystemUtils
{
  public static void loadResourcesFile()
  {
    Properties props = new Properties();
    InputStream resource = SystemUtils.class
      .getClassLoader()
      .getResourceAsStream("gemini.properties");

    if (resource != null)
    {
      try {
        props.load(resource);
      } catch (IOException error) {
        error.printStackTrace();
      }
      System.getProperties().forEach(props::put);
      System.setProperties(props);
    }
  }

  public static String getVariable(String variableName) {
    loadResourcesFile();

    if (System.getenv(variableName) != null)
      return System.getenv(variableName);
    if (System.getProperty(variableName) != null)
      return System.getProperty(variableName);

    return null;
  }
}
